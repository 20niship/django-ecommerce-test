# -*- coding: utf-8 -*-

"""
ASGI config for mysite project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

import os

from channels.routing import ProtocolTypeRouter, URLRouter
from django.core.asgi import get_asgi_application
from channels.auth import AuthMiddlewareStack
import chat.routing

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysite.settings')


# application = get_asgi_application()
application = ProtocolTypeRouter({
  "http":get_asgi_application(),
  "websocket":AuthMiddlewareStack(URLRouter(chat.routing.websocket_urlpatterns))
})

# Djangoのバージョンが古い場合、asgi.pyはデフォルトでは作られません。
# 作りましょう。
# 何を書くのかはDjango Channelsの公式Docsをみませふ。
