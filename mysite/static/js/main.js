console.log("Hello main.js")


var mapPeers = {}

var usernameInput = document.querySelector("#username")
var btnJoin = document.querySelector("#btn-join")

var username;
var webSocket;

const webSocketOnMessage = (event) => {
  var parsedData = JSON.parse(event.data);
  var peerUsername = parsedData["peer"];
  var action = parsedData["action"];

  if(username == peerUsername){
    return;
  }

  var receiver_channel_name = parsedData["message"]["receiver_channel_name"]
  if(action == "new-peer"){ // 新しいユーザーが貼ってきた時
    createOfferer(peerUsername, receiver_channel_name);
    return;
  }

  if(action == "new-offer"){
    var offer = parsedData["message"]["sdp"];
    createAnswerer(offer, peerUsername, receiver_channel_name)
    return;
  }

  if(action == "new-answer"){
    var answer = parsedData["message"]["sdp"];
    var peer = mapPeers[peerUsername][0];
    peer.setRemoteDescription(answer);
    return;
  }

  console.log("message: ", message)
}

btnJoin.addEventListener("click", () => {
  username = usernameInput.value;
  console.log("username = ", username);

  if(username === ""){
    return;
  }
  usernameInput.value = "";
  usernameInput.disabled = true;
  usernameInput.style.visibility = "hidden";

  btnJoin.disabled = true;
  btnJoin.style.visibility = "hidden";

  var labelUsername = document.querySelector("#label-username")
  labelUsername.innerHTML = username;

  var loc = window.location;
  var wsStart = "ws://";

  if(loc.protocol == "https:"){
    wsStart = "wss://";
  }
  var endPoint = wsStart + loc.host + loc.pathname;
  console.log("Endpoint = ", endPoint)
  
  webSocket = new WebSocket(endPoint); // ここでChatConsumerのインスタンスが作成され、、、

  webSocket.addEventListener("open",  (e) => {
    console.log("WebSocketの接続が開始！"       );
    sendSignal("new-peer", {})
  })
  webSocket.addEventListener("close", (e) => {console.log("WebSocketの接続が終了しました！")})
  webSocket.addEventListener("error", (e) => {console.log("WebSocketでErrorがおきました！")})

  webSocket.addEventListener("message", webSocketOnMessage)
})


var localStream = new MediaStream();
const constraints = {
  "video":true,
  "audio":true,
};
const localVideo = document.querySelector("#local-video")
const btnToggleAudio = document.querySelector("#btn-toggle-audio")
const btnToggleVideo = document.querySelector("#btn-toggle-video")

var userMedia = navigator.mediaDevices.getUserMedia(constraints)
.then(stream => {
  localStream = stream;
  localVideo.srcObject = localStream;
  localVideo.muted = true;

  var audioTracks = stream.getAudioTracks();
  var videoTracks = stream.getVideoTracks();

  audioTracks[0].enabled = true;
  videoTracks[0].enabled = true;

  btnToggleAudio.addEventListener("click",() => {
    audioTracks[0].enabled = !audioTracks[0].enabled;
    if(audioTracks[0].enabled){
      btnToggleAudio.innerHTML = "Audio Mute"
    }else{
      btnToggleAudio.innerHTML = "Audio Unmute"
    }
  })


  btnToggleVideo.addEventListener("click",() => {
    videoTracks[0].enabled = !videoTracks[0].enabled;
    if(videoTracks[0].enabled){
      btnToggleVideo.innerHTML = "Video Off"
    }else{
      btnToggleVideo.innerHTML = "Video On"
    }
  })
}).catch(error => {
  console.log("Error accessing media  : ", error)
});


var btnSendMsg = document.querySelector("#btn-send-msg")
var messageList = document.querySelector("#message-list")
var messageInput = document.querySelector("#msg")
btnSendMsg.addEventListener("click", sendMsgOnClick)

function sendMsgOnClick(){
  var message = messageInput.value;
  var li = document.createElement("li");
  li.appendChild(document.createTextNode("Me: " + message))
  messageList.appendChild(li);

  const dataChannels = getDataChannels();
  console.log(dataChannels)

  message = username + " : " + message;
  dataChannels.forEach(datachannel => {
    console.log("send message!!!")
    datachannel.send(message);
  });
  messageInput.value = "";
}

function dcOnMessage(event){
  var message = event.data;
  var li = document.createElement("li");
  li.appendChild(document.createTextNode(message));
  messageList.appendChild(li);
}




const sendSignal = (action, message) => {
  var JsonStr = JSON.stringify({
    "peer":username,
    "action":action,
    "message":message,
  })
  webSocket.send(JsonStr);
}


function createOfferer(peerUsername, receiver_channel_name){
  // 別サーバで実行したい場合は↓が変わる
  var peer = new RTCPeerConnection(null);
  AddLocalTracks(peer);

  var dc = peer.createDataChannel("channel"); /// data channel
  dc.addEventListener("open" , () => {
    console.log("connection opened!");
  });
  dc.addEventListener("message", dcOnMessage)
  // dc.onmessage = dcOnMessage;

  var remoteVideo = CreateVideo(peerUsername);
  setOnTrack(peer, peerUsername);
  console.log('Remote video source: ', remoteVideo.srcObject);

  // store the RTCPeerConnection
  // and the corresponding RTCDataChannel
  mapPeers[peerUsername] = [peer, dc];
  peer.addEventListener("iceconnectionstatechange", (event) => {
    var iceconnectionstate = peer.iceConnectionState;
    if(iceconnectionstate === "failed" || iceconnectionstate === "closed"){
      delete mapPeers[peerUsername];
      if(iceconnectionstate != "closed"){
        peer.close();
      }
      remoteVideo(remoteVideo);
    }
  });

  peer.addEventListener("icecandidate", (event) => {
    if(event.candidate){
      console.log("new ice candidate:", JSON.stringify(peer.localDescription));
      return;
    }
    sendSignal("new-offer", {
      "sdp":peer.localDescription,
      "receiver_channel_name":receiver_channel_name
    })
  })

  peer.createOffer()
  .then(o => peer.setLocalDescription(o))
  .then( () => {console.log("local description set successfully")})
}


function createAnswerer(offer, peerUsername, receiver_channel_name){
  // 別サーバで実行したい場合は↓が変わる
  var peer = new RTCPeerConnection(null);
  AddLocalTracks(peer);


  var remoteVideo = CreateVideo(peerUsername);
  setOnTrack(peer, peerUsername);

  peer.addEventListener("datachannel", e => {
    peer.dc = e.channel;
    peer.dc.addEventListener("open" , () => {
      console.log("connection opened!");
    });
    peer.dc.addEventListener("message", dcOnMessage)

    mapPeers[peerUsername] = [peer, peer.dc];
  })

  peer.addEventListener("iceconnectionstatechange", (event) => {
    var iceconnectionstate = peer.iceConnectionState;
    if(iceconnectionstate === "failed" || iceconnectionstate === "closed"){
      delete mapPeers[peerUsername];;
      if(iceconnectionstate != "closed"){
        peer.close();
      }
      removeVideo(remoteVideo);
    }
  });

  peer.addEventListener("icecandidate", (event) => {
    if(event.candidate){
      console.log("new ice candidate:", JSON.stringify(peer.localDescription));
      return;
    }
    sendSignal("new-answer", {
      "sdp":peer.localDescription,
      "receiver_channel_name":receiver_channel_name
    })
  })

  peer.setRemoteDescription(offer)
  .then(() => {
    console.log("Remote description set successfully ")
    return peer.createAnswer()
  }).then(a => {
    console.log("answer created!")
    peer.setLocalDescription(a)
  })

}


function AddLocalTracks(peer){
      // if it is not a screen sharing peer
      // add user media tracks
      localStream.getTracks().forEach(track => {
        console.log('Adding localStream tracks.');
        peer.addTrack(track, localStream);
    });
}


function CreateVideo(peerUsername){
  var videoContainer = document.querySelector("#video-container")
  var remoteVideo = document.createElement("video");
  remoteVideo.id = peerUsername + '-video'
  remoteVideo.autoplay = true;
  remoteVideo.playInline = true;

  var videoWrapper = document.createElement("div");
  videoContainer.appendChild(videoWrapper)
  videoWrapper.appendChild(remoteVideo);
  return remoteVideo;
}


function setOnTrack(peer, remoteVideo){
  console.log('Setting ontrack:');
  // create new MediaStream for remote tracks
  var remoteStream = new MediaStream();

  // assign remoteStream as the source for remoteVideo
  remoteVideo.srcObject = remoteStream;

  console.log('remoteVideo: ', remoteVideo.id);

  peer.addEventListener('track', async (event) => {
      console.log('Adding track: ', event.track);
      remoteStream.addTrack(event.track, remoteStream);
  });

  // peer.addEventListener('addstream', async (event) => {
  //   console.log('Adding track: ', event.track);
  //   remoteStream.addTrack(event.track, remoteStream);  
  // });
}

function removeVideo(Video){
  var videoWrapper = Video.parentNode;
  videoWrapper.parentNode.remoteChild(videoWrapper);

}

function getDataChannels(){
  var dataChannels = [];
  for(peerUsername in mapPeers){
      var datachannel = mapPeers[peerUsername][1];
      dataChannels.push(datachannel);
  }
  return dataChannels;
}